#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <string.h>

using namespace std;


int main(int argc, char **argv)
{
    //variables
    ifstream stream;
    string word;
    vector<string> result_Vector;
    bool reverse_Order = false;
    bool duplicate = false;

    //Check all options
    if((argc <= 3) && (argc > 1))
    {
        //Frist option
        if(!strcmp(argv[1],"--r"))
        {
            reverse_Order = true;
        }
        else if(!strcmp(argv[1],"--d"))
        {
            duplicate = true;
        }
        else
        {
            cerr << "Wrong option" << endl;
            return 0;
        }

        //Second option
        if(argc == 3)
        {
            if(!strcmp(argv[2],"--d"))
            {
                duplicate = true;
            }
            else
            {
                cerr << "Wrong option" << endl;
                return 0;
            }
        }
    }

    //Over option
    if(argc > 3)
    {
        cerr << "Too much option" << endl;
        return 0;
    }

    // Open,check and extract information from file
    stream.open("file.txt");
    if(stream)
    {
        while(getline(stream,word))
        {
            stream >> word;
            result_Vector.push_back(word);
        }
    }
    else
    {
        cerr << "ERROR: read failed!" << endl;
    }

    // Ascending sort
    sort(result_Vector.begin(),result_Vector.end());

    // Descending sort
    if(reverse_Order == true)
    {
        reverse(result_Vector.begin(),result_Vector.end());
    }

    // Remove duplicates
    if(duplicate == true)
    {
        result_Vector.erase( unique( result_Vector.begin(), result_Vector.end() ), result_Vector.end());
    }

    // Display result
    for(const auto &elem : result_Vector)
    {
        cout << elem << endl;
    }

    return 0;
}
